package ua.homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class MaskedIpInText {
    private final String path;
    private final Pattern pattern = Pattern.compile(
            "(^|\\s)(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})(\\s|$)");

    public MaskedIpInText(String path) {
        this.path = path;
    }

    public List<String> readText() {
        List<String> text = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            while (true) {
                String line = reader.readLine();
                if (line == null) break;
                text.add(maskedIp(line));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return text;
    }

    private String maskedIp(String string) {
        return string.replaceAll(pattern.toString(), "$1$2.*.*.$5$6");
    }
}
