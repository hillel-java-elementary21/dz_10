package ua.homework;

public class Main {
    public static void main(String[] args) {
        String pathA = "a.txt";
        String pathB = "b.txt";

        MaskedIpInText maskedIp = new MaskedIpInText(pathA);
        SaveText saveText = new SaveText(pathB);

        saveText.save(maskedIp.readText());
    }
}
