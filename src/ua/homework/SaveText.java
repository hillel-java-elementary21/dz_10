package ua.homework;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class SaveText {
    private final String path;

    public SaveText(String path) {
        this.path = path;
    }

    public void save(List<String> text) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            for (String s : text) {
                writer.write(s + "\n");
                writer.flush();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
